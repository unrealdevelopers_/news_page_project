<?php

    include_once 'controlador/Conexion.inc.php'; 
    
    
?>


<!DOCTYPE html>


<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge"> <!--SI SE DESEA QUE SE COMPATIBLE CON EXPLORER-->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link href=".\image\greenworldicon.ico" type="image/-icon" rel="shortcut icon" />
    <title>PUERTO NOTICIERO</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/now-ui-dashboard.css?v=1.3.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/demo/demo.css" rel="stylesheet" />

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-grid.min.css" rel="stylesheet">
    <link href="css/bootstrap-reboot.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
  </head>
  <body>
    <div class="wrapper ">
    <div class="sidebar" data-color="yellow">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
    <div class="logo">
        <a href="index.php" class="simple-text logo-mini">
          PN
        </a>
        <a href="index.php" class="simple-text logo-normal">
          PUERTO NOTICIERO
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper"><!--Menu Lateral-->
        <ul class="nav">
          <?php
            $db=Conexion::crearConexion();
            $coleccion= $db->clasificacion;
            $cursor = $coleccion->find();

            foreach ($cursor as $key => $documento) { 
              $nombreclasificacion=$documento["nombre"];

          ?>
            <li class="active">
              <a href="paginaClasificacion.php?nombreclasificacion=<?php echo  $nombreclasificacion ?>">
                <i class="now-ui-icons location_world"></i>
                <p><?php echo $documento["nombre"]; ?></p>
              </a>
            </li>
          <?php }?>

          <!--<li class="active-pro">
            <a href="./upgrade.html">
              <i class="now-ui-icons arrows-1_cloud-download-93"></i>
              Upgrade to PRO
            </a>
          </li>-->
        </ul>
      </div>
    </div>
    <div class="main-panel" id="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          
          <div class="collapse navbar-collapse justify-content-end" id="navigation"> <!--Barra de navegacion-->
            <ul id="Fecha">
              <script>

              document.getElementById('Fecha').appendChild(document.createTextNode(new Date().toString()))
              </script>
              
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="panel-header panel-header-lg" style="background-image: url('image/cieloestrellas.png')">
           <div class="jumbotron"><!--PARA HACER TITULOS O NOMBRE DE LA PAGINA-->
                <h1>PUERTO NOTICIERO<i class="now-ui-icons location_world"></i></h1>
                <p>DONDE LA INFORMACIÓN SEGURA A TODA PUERTO ORDAZ</p>
            </div>
      </div>
       <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <?php

                  $idNoticia=$_GET['idNoticia'];
                  $idnoticia=new MongoDB\BSON\ObjectId($idNoticia);
                  $db=Conexion::crearConexion();
                  $coleccion= $db->noticias;
                  $cursor = $coleccion->find(array('_id' => $idnoticia));
                  foreach ($cursor as $key => $documento) {
                    
              ?>

                <h4 class="card-title"><?php echo $documento["titulo"];  ?></h4>
                <h5 class="card-title"><?php echo $documento["fecha_noticia"];  ?></h4>
              </div>

              <div class="card-body">
                <?php 
                  $cuerpo=$documento["cuerpo"]; } 
                  $cortes=explode(".",$cuerpo);
                  $parrafo="";
                  $relevancia=1;
                  $numparrafo=0;
                  $contparrafo=0;
                  $parrafomostrar="";

                 for($i=0;$i<count($cortes);$i++){
                    
                    if(count($cortes)==1){
                      $parrafomostrar=$cortes[0];
                    }else if($contparrafo<3){
                      $parrafo=$parrafo.$cortes[$i];
                      $contparrafo++;
                    }else{
                      $parrafomostrar=$parrafo;
                      $relevancia=$relevancia+1;
                      $parrafo="";
                      $contparrafo=0;
                    }
                    
                    $coleccion2= $db->archivosMultimedia;

                    $cursor2 = $coleccion2->findOne(array('idnoticia' => $idnoticia, 'relevancia' => $relevancia));
                    if(!is_null($cursor2)){
                      $idimag=$cursor2["idrecurso"];
                      $id=new MongoDB\BSON\ObjectId($idimag);
                      $dbaux=Conexion::crearConexion();
                      $bucketOptions = (new \Helmich\GridFS\Options\BucketOptions)
                        ->withBucketName('fs');
                      $bucket = new \Helmich\GridFS\Bucket($dbaux, $bucketOptions);
                      $file=$bucket->find(['_id' => $id],(new \Helmich\GridFS\Options\FindOptions)->withLimit(1));
                      foreach ($file as $key => $documento2) {
                          $file2=$documento2;
                      }
                      $fileStream=fopen('prueba.bin', 'w');
                      $downloadStream=$bucket->downloadToStream($file2['_id'],$fileStream);
                      
                      
                     $con=file_get_contents("prueba.bin");
                     $en=base64_encode($con);
                     $mime='image/jpg';
                     $binary_data='data: '.$mime.'; base64,'.$en;
                      
                
                ?>
                 <img style="display: block; margin: auto;" height="300px" width="300px" src="<?php echo $binary_data; ?>">
               <?php } ?>
                <br/>
                <p><?php if($parrafomostrar!=""){echo $parrafomostrar;  $parrafomostrar="";} } ?></p>
                </div>

              </div>
            </div>
          </div>
          
      <footer class="footer">
        <div class="container-fluid">
          <nav>
            <ul>
              <li>
                <a href="index.php">
                  <h5>Noticia Realizada Por:</h5>
                  <?php
                    $coleccion= $db->historialnoticiasreportero;
                    $cursor=$coleccion->find(array('idNoticia' => $idnoticia));
                    foreach ($cursor as $key => $documento) {
                        $coleccion=$db->reporteros;
                        $reportero=$coleccion->findOne(array("cedula" => $documento['cedula']));

                    
                  ?>
                  <h6><?php echo "Nombre: ".$reportero['nombre']." ".$reportero['apellido']." CI: ".$reportero['cedula']; ?></h6>
                  <br/>
                <?php } ?>
                  Puerto Noticia
                </a>
              </li>
             
            </ul>
          </nav>
          <div class="copyright" id="copyright">
            &copy;
            <script>
              document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>
            
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js"></script>
  <script src="assets/js/core/popper.min.js"></script>
  <script src="assets/js/core/bootstrap.min.js"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Chart JS -->

  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-dashboard.min.js?v=1.3.0" type="text/javascript"></script>

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  </body>
</html>