<?php
    include_once 'controlador/Conexion.inc.php'; 

    include __DIR__ . '/vendor/autoload.php';
    require_once 'vendor/autoload.php';
    use League\Flysystem\Adapter\AbstractAdapter;
    use League\Flysystem\AdapterInterface;
    use League\Flysystem\Config;
    use League\Flysystem\NotSupportedException;
    use League\Flysystem\SafeStorage;
    use League\Flysystem\Adapter\Polyfill\StreamedCopyTrait;
    use League\Flysystem\Util\MimeType;
    use League\Flysystem\Adapter\Polyfill\NotSupportingVisibilityTrait;
    use League\Flysystem\Adapter\Polyfill\StreamedReadingTrait;
    use League\Flysystem\FilesystemInterface;
    use League\Flysystem\PluginInterface;;
    use League\Flysystem\Util;
    use MongoDB\GridFS\Bucket;
    use MongoDB\GridFS\CollectionWrapper;
    use MongoDB\GridFS\ReadableStream;

    $nombreClasificacion=$_GET['nombreclasificacion'];
    $db=Conexion::crearConexion();
    $coleccion= $db->clasificacion;
    $cursor = $coleccion->find(array('nombre' => $nombreClasificacion),array('_id'));
    foreach ($cursor as $key => $documento) {
      $idClasificacion=$documento["_id"];
    }
    
?>

<!DOCTYPE html>


<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge"> <!--SI SE DESEA QUE SE COMPATIBLE CON EXPLORER-->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link href=".\image\greenworldicon.ico" type="image/-icon" rel="shortcut icon" />
    <title>PUERTO NOTICIERO</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/now-ui-dashboard.css?v=1.3.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/demo/demo.css" rel="stylesheet" />

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-grid.min.css" rel="stylesheet">
    <link href="css/bootstrap-reboot.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="wrapper ">
    <div class="sidebar" data-color="red">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
    <div class="logo">
        <a href="index.php" class="simple-text logo-mini">
          PN
        </a>
        <a href="index.php" class="simple-text logo-normal">
          PUERTO NOTICIERO
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper"><!--Menu Lateral-->
        <ul class="nav">
          <?php
            
            $db=Conexion::crearConexion();
            $coleccion= $db->clasificacion;
            $cursor = $coleccion->find();

            foreach ($cursor as $key => $documento) { 
              $nombreclasificacion=$documento["nombre"];

          ?>
            <li class="active">
              <a href="paginaClasificacion.php?nombreclasificacion=<?php echo  $nombreclasificacion ?>">
                <i class="now-ui-icons location_world"></i>
                <p><?php echo $documento["nombre"]; ?></p>
              </a>
            </li>
          <?php }?>

          <!--<li class="active-pro">
            <a href="./upgrade.html">
              <i class="now-ui-icons arrows-1_cloud-download-93"></i>
              Upgrade to PRO
            </a>
          </li>-->
        </ul>
      </div>
    </div>
    <div class="main-panel" id="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          
          <div class="collapse navbar-collapse justify-content-end" id="navigation"> <!--Barra de navegacion-->
            <ul id="Fecha">
              <script>

              document.getElementById('Fecha').appendChild(document.createTextNode(new Date().toString()))
              </script>
              
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="panel-header panel-header-lg" style="background-image: url('image/cieloestrellas.png')">
           <div class="jumbotron"><!--PARA HACER TITULOS O NOMBRE DE LA PAGINA-->
                <h1>PUERTO NOTICIERO<i class="now-ui-icons location_world"></i></h1>
                <p>DONDE LA INFORMACIÓN SEGURA A TODA PUERTO ORDAZ</p>
            </div>
      </div>
       <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Noticias de <?php echo $nombreClasificacion;?></h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th >
                        Foto
                      </th>
                      <th >
                        Titulo
                      </th>
                      <th >
                        Resumen
                      </th>
                      <th >
                        Clasificacion
                      </th>
                      <th >
                        Fecha
                      </th>
                      <th >
                        Enlace
                      </th>
                    </thead>

                    <tbody>
                      <?php 
                        $coleccion= $db->noticias;
                        $id=new MongoDB\BSON\ObjectId($idClasificacion);
                       $cursor = $coleccion->find(array('idClasificacion' => $id));
                        foreach ($cursor as $key => $documento) {
                        
                      ?>
                      <tr>
                        <td>
                          <?php 

                            $coleccion2= $db->archivosMultimedia;
                            $id=new MongoDB\BSON\ObjectId($documento["_id"]);

                            $cursor2 = $coleccion2->find(array('idnoticia' => $id, 'relevancia' => 1));
                            foreach ($cursor2 as $key => $documento2) {
                                $idimag=$documento2["idrecurso"];
                                $id=new MongoDB\BSON\ObjectId($idimag);
                            }
                            $dbaux=Conexion::crearConexion();
                            $bucketOptions = (new \Helmich\GridFS\Options\BucketOptions)
                              ->withBucketName('fs');
                            $bucket = new \Helmich\GridFS\Bucket($dbaux, $bucketOptions);
                            $file=$bucket->find(['_id' => $id],(new \Helmich\GridFS\Options\FindOptions)->withLimit(1));
                            foreach ($file as $key => $documento2) {
                                $file2=$documento2;
                            }
                            $fileStream=fopen('prueba.bin', 'w');
                            $downloadStream=$bucket->downloadToStream($file2['_id'],$fileStream);
                            
                            
                           $con=file_get_contents("prueba.bin");
                           $en=base64_encode($con);
                           $mime='image/jpg';
                           $binary_data='data: '.$mime.'; base64,'.$en;
                                   
                          ?>
                          <img height="100px" width="100px" src="<?php echo $binary_data;?>">
                          
                        </td>
                        <td>
                          <?php echo $documento["tituloClave"]; ?>
                        </td>
                        <td>
                          <?php echo $documento["sintesis"]; ?>
                        </td>
                        <td >
                          <?php echo $nombreClasificacion; ?>
                        </td>
                        <td >
                          <?php echo $documento["fecha_noticia"]; ?>
                        </td>
                        <td >
                          <?php $idNoticia=$documento["_id"]; ?>
                          <a href="paginaNoticia.php?idNoticia=<?php echo  $idNoticia ?>">Ir</a>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
      <footer class="footer">
        <div class="container-fluid">
          <nav>
            <ul>
              <li>
                <a href="index.php">
                  Puerto Noticia
                </a>
              </li>
             
            </ul>
          </nav>
          <div class="copyright" id="copyright">
            &copy;
            <script>
              document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>
            
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js"></script>
  <script src="assets/js/core/popper.min.js"></script>
  <script src="assets/js/core/bootstrap.min.js"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Chart JS -->

  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/now-ui-dashboard.min.js?v=1.3.0" type="text/javascript"></script>
  <script type="text/javascript" src="app.js"></script>
  <script src="js/jquery.min.js"></script>
  <script src="app.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  </body>
</html>